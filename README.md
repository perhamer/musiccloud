# musicCloud获取用户评论
    由于数据量的问题，仅提供查询用户所有歌单及收藏的歌单下所有歌曲的评论
    待优化：只查询用户注册时间之后的评论

需要下载chromeDriver.exe

执行顺序

    1、playlist_by_user.py
    2、musics_by_playlist.py
    3、comment_by_music.py
    
    
# sql init:

DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `music_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `reply_comment_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '回复的评论id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;



DROP TABLE IF EXISTS `playlist_music`;
CREATE TABLE `playlist_music`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `playlist_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `music_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `music_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`, `playlist_id`, `music_id`) USING BTREE,
  INDEX `playlist_id`(`playlist_id`, `music_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2915 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;


DROP TABLE IF EXISTS `user_playlist`;
CREATE TABLE `user_playlist`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `playlist_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `playlist_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`, `user_id`, `playlist_id`) USING BTREE,
  INDEX `user_id`(`user_id`, `playlist_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;